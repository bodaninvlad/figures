package com.stormnet.figuresFx.controller;

import com.stormnet.figuresFx.figures.*;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

import java.net.URL;
import java.util.ArrayList;
import java.util.Random;
import java.util.ResourceBundle;

public class Controller implements Initializable {


    private ArrayList<Figure> figures;
    private Random random;
    private Drawer drawer;

    @FXML
    private Canvas inputCancas;
    @FXML
    private Canvas canvas;

    private ArrayList<Double> arrayListX;
    private ArrayList<Double> arrayListY;

    private Figure createFigure(double x, double y) {
        Figure figure = null;

        switch (random.nextInt(5)) {
            case Figure.FIGURE_TYPE_CIRCLE: {
                figure = new Circle(random.nextInt(10) + 5, Color.rgb(random.nextInt(100),
                        random.nextInt(100), random.nextInt(100)), x, y, random.nextInt(100));

                break;
            }
            case Figure.FIGURE_TYPE_RECTANGLE: {
                figure = new Rectangle(random.nextInt(10) + 5, Color.rgb(random.nextInt(100),
                        random.nextInt(100), random.nextInt(100)), x, y, random.nextInt(100), random.nextInt(100));

                break;
            }
            case Figure.FIGURE_TYPE_TRIANGLE: {
                figure = new Triangle(random.nextInt(10) + 5, Color.rgb(random.nextInt(100),
                        random.nextInt(100), random.nextInt(100)), x, y, random.nextInt(100));
                break;
            }
            case Figure.FIGURE_TYPE_SIN: {
                figure = new Sin(random.nextInt(10) + 5, Color.rgb(random.nextInt(100),
                        random.nextInt(100), random.nextInt(100)), x, y);
                break;
            }
            case Figure.FIGURE_TYPE_SOME_FIGURE: {
                if (arrayListX!=null&&arrayListY!=null)
                    figure = new SomeFigure(random.nextInt(10) + 5, Color.rgb(random.nextInt(100),
                            random.nextInt(100), random.nextInt(100)), x - SomeFigurePattern.getCx(), y - SomeFigurePattern.getCy(), SomeFigurePattern.getxArray(), SomeFigurePattern.getyArray());
                break;
            }
        }
        return figure;
    }


    private void repaint() {
        canvas.getGraphicsContext2D().clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        for (Figure figure : figures) {
            if (figure != null) {
                drawer.draw(figure);
            }
        }
    }


    @FXML
    private void onMouseClicked(MouseEvent event) {
        figures.add(createFigure(event.getX(), event.getY()));
        repaint();
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        drawer = new Drawer(canvas.getGraphicsContext2D());
        figures = new ArrayList<>();
        random = new Random(System.currentTimeMillis());
    }

    @FXML
    private void onMouseDragged(MouseEvent event) {
        GraphicsContext gc = inputCancas.getGraphicsContext2D();


        gc.setFill(Color.BLACK);
        arrayListX.add(event.getX());
        arrayListY.add(event.getY());
        gc.fillRect(event.getX(), event.getY(), 5, 5);
    }

    public void clean(MouseEvent event) {
        arrayListX = new ArrayList<>();
        arrayListY = new ArrayList<>();
        inputCancas.getGraphicsContext2D().clearRect(0, 0, inputCancas.getWidth(), inputCancas.getHeight());
    }

    public void createPattern(MouseEvent event) {
        SomeFigurePattern.setFigure(arrayListX, arrayListY);
    }
}

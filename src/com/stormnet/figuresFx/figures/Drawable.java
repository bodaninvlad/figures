package com.stormnet.figuresFx.figures;


public interface Drawable<T extends Figure> {
    void draw(T t);
}

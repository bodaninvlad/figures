package com.stormnet.figuresFx.figures;

import javafx.scene.canvas.GraphicsContext;

public class Drawer<T extends Figure> implements Drawable<T> {

    private GraphicsContext graphicsContext;

    public Drawer(GraphicsContext graphicsContext) {
        this.graphicsContext = graphicsContext;
    }

    @Override
    public void draw(T figure) {
        figure.draw(graphicsContext);
    }
}

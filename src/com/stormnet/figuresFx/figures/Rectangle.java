package com.stormnet.figuresFx.figures;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.Objects;

public class Rectangle extends Figure {

    private double height;
    private double width;

    public Rectangle(double lineWidth, Color color, double x, double y, double height, double width) {
        this(lineWidth, color, x, y);
        this.height = height;
        this.width = width;
    }

    private Rectangle(double lineWidth, Color color, double x, double y) {
        super(lineWidth, color, x, y, FIGURE_TYPE_RECTANGLE);
    }

    @Override
    public void draw(GraphicsContext gc) {
        gc.setLineWidth(lineWidth);
        gc.setStroke(color);
        gc.strokeRect(x - width / 2, y - height / 2, width, height);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rectangle rectangle = (Rectangle) o;
        return Double.compare(rectangle.height, height) == 0 &&
                Double.compare(rectangle.width, width) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(height, width);
    }
}

package com.stormnet.figuresFx.figures;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.Objects;

public class SomeFigure extends Figure {
    private ArrayList<Double> xArray;
    private ArrayList<Double> yArray;

    public SomeFigure(double lineWidth, Color color, double x, double y, ArrayList<Double> arrX, ArrayList<Double> arrY) {
        super(lineWidth, color, x, y, FIGURE_TYPE_SOME_FIGURE);
        xArray = arrX;
        yArray = arrY;
    }

    private double findMinX() {
        double mixX = xArray.get(0);
        for (int i = 1; i < xArray.size(); i++) {
            if (mixX > xArray.get(i))
                mixX = xArray.get(i);
        }
        return mixX;
    }

    private double findMinY() {
        double minY = yArray.get(0);
        for (int i = 1; i < yArray.size(); i++) {
            if (minY > yArray.get(i))
                minY = yArray.get(i);
        }
        return minY;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SomeFigure that = (SomeFigure) o;
        return Objects.equals(xArray, that.xArray) &&
                Objects.equals(yArray, that.yArray);
    }

    @Override
    public int hashCode() {
        return Objects.hash(xArray, yArray);
    }

    @Override
    public void draw(GraphicsContext gc) {
        gc.setLineWidth(lineWidth);
        gc.setFill(color);
        for (int i = 0; i < xArray.size(); i++) {
            gc.fillRect(x + xArray.get(i) - findMinX(), y + yArray.get(i) - findMinY(), lineWidth, lineWidth);
        }
    }
}
